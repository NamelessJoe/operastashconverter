import sqlite3
import time
import sys

	#connect to db, get 2 values: c0url,c1title
connection = sqlite3.connect("stash.db")
cursor = connection.cursor()
cursor.execute("Select c0url,c1title from stash_fts_content")

	#create file, write the header
with open('bookmarks.html', 'w') as file:

	file.write("<!DOCTYPE NETSCAPE-Bookmark.file-1>\n")
	file.write("<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html\"; charset=UTF-8\n<TITLE>Bookmarks</TITLE>\n<H1>Bookmarks</H1>\n")

	#write the starting html tags, choose a folder name between "H3" - tags 

	file.write("<DL>\n<p>\n\t<DT><H3>importedFromOpera</H3>\n\n\t<DL><p>\n")
	
	#loop through db and write data into html tags 
	
	for row in cursor:

		file.write("\t\t<DT><A HREF=\"{}\">{}</A>\n\t\t".format(row[0],row[1].encode("UTF-8")))

		#write ending tags, that's it

	file.write("\t</DL><p>\n</DL>")
