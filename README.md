# README #



### What is this repository for? ###

* This repo contains a small python script used to convert a "Stash.db" - File from the Opera Browser into an HTML File importable by Firefox.



### Usage ###

Just put the Stash.db in the same directory as the script and run the script. An HTML file will be generated in that directory.